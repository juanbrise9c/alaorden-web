/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var s = {}
var api = 'https://ohuirasoft.net/api/_delivery/?'
var token = localStorage.getItem('ojoDeDiosDelivery')
var deliveryAddress = JSON.parse(localStorage.getItem('deliveryAddress'))

var getFilter = '_GET&filter='
var patchFilter = '_PATCH&filter='
var postFilter = '_POST&filter='
var u = '________u='


$(function() {
    FastClick.attach(document.body);
});


function goApp(){
    jQuery(document).ready(function($) {
        app.token = JSON.parse(localStorage.getItem('tokenDelivery'));
        $('#splashScreen div>svg')
            .css({
                'opacity': 1,
                'transform': 'scale(1)'
            })
            .wait(3000)
            .css({
                'opacity': 0,
                'transform': 'scale(.9)'
            })

        $('#splashScreen')
            .wait(3300)
            .css('transform', 'translateY(-100vh)')
            .wait(200)
            .hide()
        riot.compile(function() {
            riot.mount('index');
            riot.mount('.viewRight');
            if (app.token==null) {
                riot.mount('welcome');
            }else{
                $('welcome').remove()
            }
            // riot.mount('login');
            // riot.mount('code');
        });
    });
}

var app = {
    // Application Constructor
    initialize: function() {
        try{
            cordova;
            document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        }catch(e){
            goApp()
        }
    },

    onDeviceReady: function() {
        this.receivedEvent('deviceready');
        goApp()
    },

    receivedEvent: function(id) {
        console.log('Received Event: ' + id);

    }
};

app.initialize();

var hashBlock = 0
window.addEventListener("hashchange", function() {
    if (hashBlock)
        return false;

    if (window.location.hash == '#/' || window.location.hash == '') {
        $('index>div').css('transform', 'translateX(0px)');
        $('.viewRight').css('transform', 'translateX(100vw)');
    } else if (window.location.hash == '#/orders') {
        $('index>div').css('transform', 'translateX(-100vw)');
    }  else if (window.location.hash == '#/profile') {
        $('index>div').css('transform', 'translateX(-200vw)');
    } else if (window.location.hash == '#/affiliate') {
        $('affiliate').css('transform', 'translateX(0vw)');
        $('item, myOrder').css('transform', 'translateX(100vw)');
    } else if (window.location.hash == '#/affiliate/item') {
        $('item').css('transform', 'translateX(0vw)');
    } else if (window.location.hash == '#/categories') {
        $('categories').css('transform', 'translateX(0vw)');
        $('affiliate, item').css('transform', 'translateX(100vw)');
    }  else if (window.location.hash == '#/results') {
        $('results').css('transform', 'translateX(0vw)');
        $('affiliate, item').css('transform', 'translateX(100vw)');
    }else if (window.location.hash == '#/my-order') {
        $('myOrder').css('transform', 'translateX(0vw)');
        $('item, ubication, wayToPay, typeOfService').css('transform', 'translateX(100vw)');
    }else if (window.location.hash == '#/ubication') {
        s.ubication.check()
        $('ubication').css('transform', 'translateX(0vw)');
    }else if (window.location.hash == '#/my-order/wayToPay') {
        $('wayToPay').css('transform', 'translateX(0vw)');
    }else if (window.location.hash == '#/my-order/typeOfService') {
        $('typeOfService').css('transform', 'translateX(0vw)');
    }



}, false);


