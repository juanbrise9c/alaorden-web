var G = {}
G.token = {}
G.idOnesignal = ''
G.affiliate = {}
G.product = {}


G.setNavbar = (navBar) =>{
	$('#navbars').html('<'+navBar+'></'+navBar+'>')
	riot.mount(navBar)
}

G.goHash = (element) =>{
	$('#navbars').html('<navbar-'+element+'></navbar-'+element+'>')
	$('#pages').html('<'+element+'></'+element+'>')
	riot.mount('navbar-'+element)
	riot.mount(element)

	if (window.innerWidth < 800)
		app.panel.close('aside.panel')
}


G.openPopUp = (tag) =>{
	popup.open()
	$('#popup').html('<popup-'+tag+'></popup-'+tag+'>')
	riot.mount('popup-'+tag)
}

G.goTab = (_case) => {
	$('#index-tabs .tab-active').removeClass('tab-active')
	$('#index-footer .tab-link-active').removeClass('tab-link-active')
	$('#navbar-index,#navbar-saved,#navbar-orders,#navbar-account').hide()
	$('#index-toolbar').hide()
	switch(_case){
		case 'home':
			$('#index-tabs .tab').eq(0).addClass('tab-active')
			$('#index-footer .tab-link').eq(0).addClass('tab-link-active')
			$('#index-footer .tab-link-highlight').css('transform', 'translate3d(0%, 0px, 0px)')
			$('#navbar-index').show()
			$('#index-toolbar').show()
		break

		case 'saved':
			$('#index-tabs .tab').eq(1).addClass('tab-active')
			$('#index-footer .tab-link').eq(1).addClass('tab-link-active')
			$('#index-footer .tab-link-highlight').css('transform', 'translate3d(100%, 0px, 0px)')
			$('#navbar-saved').show()
			$('#saved-index').show()
		break

		case 'orders':
			$('#index-tabs .tab').eq(2).addClass('tab-active')
			$('#index-footer .tab-link').eq(2).addClass('tab-link-active')
			$('#index-footer .tab-link-highlight').css('transform', 'translate3d(200%, 0px, 0px)')
			$('#navbar-orders').show()
			$('#orders-index').show()
		break

		case 'account':
			$('#index-tabs .tab').eq(3).addClass('tab-active')
			$('#index-footer .tab-link').eq(3).addClass('tab-link-active')
			$('#index-footer .tab-link-highlight').css('transform', 'translate3d(300%, 0px, 0px)')
			$('#navbar-account').show()
			$('#account-index').show()
		break
	}
}


G.goAffiliate = () => {
	hash('affiliate')

	var filter = {
		table: 'products',
		where: {
			status: 1,
			inApp: 1,
			idAffiliate: G.affiliate.id
		}
	}
	$.ajax({
		url: api+getFilter+encodeURIComponent(JSON.stringify(filter))+'&hook=statisticsAffiliate',
		type: 'GET',
		dataType: 'json'
	})
	.done(function(r) {
		G.affiliate.products = r
		G.affiliate.productCategories = JSON.parse(G.affiliate.productCategories)
		G.affiliate.questionnaires = JSON.parse(G.affiliate.questionnaires)
		for (var i = 0; i < G.affiliate.productCategories.length; i++) {
			G.affiliate.productCategories[i].products = []
			for (var k = 0; k < G.affiliate.products.length; k++) {
				if (G.affiliate.products[k].idsCategories.indexOf(G.affiliate.productCategories[i].id) !== -1) {
					G.affiliate.productCategories[i].products.push(G.affiliate.products[k])
				}
			}
			G.affiliate.productCategories[i].products = G.affiliate.productCategories[i].products.sort(function (a, b) {
				   return a.level - b.level;
			});
		}
		s.affiliate.productCategories = G.affiliate.productCategories
		s.affiliate.update()
	})
}


G.goProduct = () => {
	hash('affiliate/product')
	riot.mount('product')

	$('product').wait(300).removeClass('page-next').addClass('page-current')
	$('#root-append')
		.addClass('router-transition-f7-parallax-forward router-transition-custom')
		.wait(300)
		.removeClass('router-transition-f7-parallax-forward router-transition-custom')

	$('affiliate')
		.wait(300)
		.removeClass('page-current')
		.addClass('page-previous')

	var filter = {
		table: 'products',
		where: {
			id: G.product.id
		}
	}
	$.ajax({
		url: api+getFilter+encodeURIComponent(JSON.stringify(filter))+'&hook=statisticsProduct',
		type: 'GET',
		dataType: 'json'
	})
	.done(function(r) {
		G.product = r[0]
		G.product.finalPrice = 0
		// s.product.isEdit = 0
		// s.product.txtSubmit = 'Agregar'
		G.product.idsQuestionnaire = G.product.idsQuestionnaire.split(',')
		G.product.questionnaires = []
		for (var i = 0; i < G.product.idsQuestionnaire.length; i++) {
			var x = G.product.idsQuestionnaire[i]
			for (var k = 0; k < G.affiliate.questionnaires.length; k++) {
				var y = G.affiliate.questionnaires[k]
				if (x == y.id) {
					G.product.questionnaires.push(y)
					break;
				}
			}
		}
		s.product.update()
	})
}

G.leaveProduct = () => {
	if (!("name" in G.product))
		return true

	G.product = {}
	$('#root-append')
		.addClass('router-transition-f7-parallax-backward router-transition-custom')
		.wait(300)
		.removeClass('router-transition-f7-parallax-backward router-transition-custom')

	$('affiliate')
		.wait(300)
		.removeClass('page-previous')
		.addClass('page-current')

	$('product').wait(300).html('').removeAttr('class')
}

G.leaveAffiliate = () => {
	if (!("name" in G.affiliate))
		return true
	
	G.affiliate = {}
	$('#root-append')
		.addClass('router-transition-f7-parallax-backward router-transition-custom')
		.wait(300)
		.removeClass('router-transition-f7-parallax-backward router-transition-custom')

	$('#page-index')
		.wait(300)
		.removeClass('page-previous')
		.addClass('page-current')

	$('affiliate').wait(300).html('').removeAttr('class')
}


G.indexFromAffiliate = () =>{
	console.log('BACK')
	G.isBack=1
	window.history.back()
	if ($('index').html()=='') {
		console.log('MOUNT')
		riot.mount('index')

		$ptrIndexContent = $$('#tab-index .ptr-content');
		$ptrIndexContent.on('ptr:refresh', function (e) {
				$('#index-page').css('overflow-y', 'hidden')
	        	s.index.filter.skip = 1
				s.index.loadData(true)
		})
	}
}

G.showSearchbar = (el) =>{
	$('navbar-'+el+' .searchbar')
		.addClass('searchbar searchbar-expandable searchbar-components searchbar-init searchbar-focused searchbar-enabled')
		.wait(300)
		.find('input')
		.focus()
	
	$('navbar-'+el+' .navbar')
		.addClass('navbar navbar-large navbar-transparent navbar-large-collapsed with-searchbar-expandable-enabled')

	$('.searchbar-backdrop').addClass('searchbar-backdrop-in')
}

G.hideSearchbar = (el) =>{
	$('navbar-'+el+' .searchbar')
		.attr('class', 'searchbar searchbar-expandable searchbar-components searchbar-init')

	$('navbar-'+el+' .navbar')
		.attr('class', 'navbar navbar-large navbar-transparent navbar-large-collapsed')

	$('.searchbar-backdrop')
		.removeClass('searchbar-backdrop-in')
}


G.openSearchIndex = () =>{
	app.tab.show('#tab-index')
	$('#searchbar-backdrop-index').addClass('searchbar-backdrop-in')
}

function shuffle(a){
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}


var toastWithCustomButton
G.toast = (text, position, duration) =>{
	toastWithCustomButton = app.toast.create({
		text: text,
		position: position ? position :'bottom',
		closeTimeout: duration ? duration : 3000,
		closeButtonText: '<i class="icon icon-x-circle"></i>',
		closeButtonColor: 'red',
	})

	return toastWithCustomButton.open()
}

G.logout = () => {
	localStorage.removeItem('afiliateTokenDelivery')
	token = null
	loginScreen.open()
}

var notificationClickToClose 
G.notification = ()=>{
	var notificationClickToClose = app.notification.create({
		icon: '<i class="icon icon-bell"></i>',
		title: 'ALAORDEN',
		titleRightText: 'now',
		subtitle: 'Hay un nuevo mensaje',
		text: 'Presiona para cerrar',
		closeOnClick: true,
		closeTimeout: 1500,
		on: {
			click: function () {
			  G.goHash('chat');
			}
		}
	})
	return notificationClickToClose.open()
}

