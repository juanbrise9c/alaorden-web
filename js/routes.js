var routes = [
	{
		path: '/',
		name: 'index',
		// on: {
		// 	pageAfterIn: function(argument) {
		// 		if ($('index').html() == '')
		// 			riot.mount('index')
		// 	}
		// }
	},
	{
		path: '/orders',
		name: 'orders',
		content: `
			<div class="page page-current">
				<orders></orders>
			</div>
		`,
		options: {
	      transition: 'f7-parallax',
	    },
	    on: {
	    	pageBeforeIn: function (e, page) {
	    		if ($('affiliate').html() == '')
	    			riot.mount('affiliate')
	        },
	        pageAfterIn: function (event, page) {
	        	$('affiliate').removeClass('page-previous')
	        }
	    }
	},
	{
		path: '/affiliate',
		name: 'affiliate',
		content: `
			<div class="page page-current">
				<affiliate></affiliate>
			</div>
		`,
		options: {
	      transition: 'f7-parallax',
	    },
	    on: {
	    	pageBeforeIn: function (e, page) {
	    		if ($('affiliate').html() == '')
	    			riot.mount('affiliate')
	        },
	        pageAfterIn: function (event, page) {
	        	$('affiliate').removeClass('page-previous')
	        }
	    }
	},
	{
		path: '/affiliate/product',
		name: 'product',
		content: `
			<div class="page page-current">
				<product></product>
			</div>
		`,
		options: {
	      transition: 'f7-parallax',
	    },
	    on: {
	    	pageBeforeIn: function test (e, page) {
	    		riot.mount('product')
	        }
	    }
	}
]