var app,
	view,
	$$,
	$ptrordersContent,
	popup,
	loginScreen,
	token

jQuery(document).ready(function($) {
	app = new Framework7({
		root: '#app',
		theme : 'md',
		name: 'OrdenaYa',
		data: function() {},
		methods: {},
		routes: routes,
		toolbar: {
			hideOnPageScroll: true,
		}
	})

	$$ = Dom7
	view = app.views.create('.view-main')
	app.searchbar.create({
						backdrop: true,
						backdropEl: '#searchbar-backdrop-index',
						expandable:true,
						el: '#searchbar-index',
					})

	popup = app.popup.create({
		content: '<div class="popup" id="popup"></div>',
		on: {
			opened: function () {
				console.log('Popup opened')
			}
		}
	})

	loginScreen = app.loginScreen.create({
		content: '<div class="login-screen"><login></login></div>',
		on: {
		  open: function () {
			riot.mount('login')
		  }
		}
	  })
})