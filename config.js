var s = {}
var api = 'http://localhost:3000/api/'

var token = JSON.parse(localStorage.getItem('user'))

jQuery(document).ready(function($) {
    FastClick.attach(document.body)
    riot.mount('index')
    riot.mount('aside')
    setTimeout(function() {
        G.setNavbar('navbar-index')
        app.panel.open('aside.panel')
    }, 500)
})

function hash(_hash) {
	window.location.hash = '#/'+_hash
}

document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    window.open = cordova.InAppBrowser.open;
}

$(window).on('hashchange', function(e) {
    var h = window.location.hash

    switch(h){
    	case '#/': case '#/index': case '':
            G.goHash('index')
        break;
    	case '#/chat':
            G.goHash('chat')
        break;
    }
});

var countNotification;
function calculateNotfication() {
    let filter = {
        where: {
            author: {
                neq: token ? token.nameContact : 'mesa'
            },
            isRead: false
        }
    }

    fetch(api+"/Messages?filter="+encodeURIComponent(JSON.stringify(filter)), {
        method: 'GET',
        headers: {'Content-Type': 'application/json'}
    })
    .then(response => response.json())
    .then(data => {
        countNotification = data.length;
        console.log(countNotification);
    })
    .catch((error) => {
        console.error('Error:', error);
    });
}

calculateNotfication();

if (token) {
    hash('mesero');
}